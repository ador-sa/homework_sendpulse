<?php

class Route {

    public static function set( $route, $function ) {
        $url   = !empty( $_GET[ 'url' ] ) ? $_GET[ 'url' ] : '';
        $parts = explode( '/', $url );
        if ( ( empty( $parts[ 0 ] ) && empty( $route ) ) || ( !empty( $parts[ 0 ] ) && $parts[ 0 ] == $route ) ) {
            $function->__invoke( isset( $parts[ 1 ] ) ? $parts[ 1 ] : null );
        }
    }

}