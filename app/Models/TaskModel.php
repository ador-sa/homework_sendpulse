<?php

class TaskModel extends Model {

    public function create( $data ) {
        $this->db->query( "INSERT INTO tasks SET name='" . $this->db->escape( $data[ 'name' ] ) . "', body='" . $this->db->escape( $data[ 'body' ] ) . "', complete_till='" . $this->db->escape( $data[ 'complete_till' ] ) . "', is_completed='" . $this->db->escape( $data[ 'is_completed' ] ) . "',  user_id='" . intval( $data[ 'user_id' ] ) . "', parent_id='" . intval( $data[ 'parent_id' ] ) . "'" );
    }

    public function update( $id, $data ) {

        $this->db->query( "UPDATE tasks SET name='" . $this->db->escape( $data[ 'name' ] ) . "', body='" . $this->db->escape( $data[ 'body' ] ) . "', complete_till='" . $this->db->escape( $data[ 'complete_till' ] ) . "', is_completed='" . $this->db->escape( $data[ 'is_completed' ] ) . "',  user_id='" . intval( $data[ 'user_id' ] ) . "', parent_id='" . intval( $data[ 'parent_id' ] ) . "' WHERE id='" . intval( $id ) . "'" );

        if ( $data[ 'is_completed' ] && !empty( $data[ 'parent_id' ] ) ) {
            $this->checkCompleted( $data[ 'parent_id' ] );
        }
    }

    /** Complete or uncomplete task and conected tasks
     * @param $id integer
     * @param $is_cmpleted boolean
     * @return array
     */
    public function complete( $id, $is_cmpleted ) {
        $data = [
            'completed'   => [],
            'uncompleted' => []
        ];

        $this->db->query( "UPDATE tasks SET is_completed='" . intval( $is_cmpleted ) . "' WHERE id='" . intval( $id ) . "'" );
        $task = $this->getTask( $id );
        if ( $is_cmpleted ) {
            $data[ 'completed' ] = $this->setCompleted( $task );
        } else {
            $data[ 'uncompleted' ] = $this->setUncompleted( $task );
        }

        return $data;
    }

    public function setCompleted( $task ) {
        $ids = [];
        if ( $task[ 'parent_id' ] ) {
            $query = $this->db->query( "SELECT * FROM tasks WHERE parent_id='" . intval( $task[ 'parent_id' ] ) . "' AND is_completed=0" );
            if ( count( $query->rows ) == 0 ) {
                $this->db->query( "UPDATE tasks SET is_completed='1' WHERE id='" . intval( $task[ 'parent_id' ] ) . "'" );
                $ids[] = [ 'id' => $task[ 'parent_id' ] ];
            }
        } else {
            $query = $this->db->query( "SELECT id FROM tasks WHERE parent_id='" . intval( $task[ 'id' ] ) . "'" );
            if ( count( $query->rows ) ) {
                $this->db->query( "UPDATE tasks SET is_completed='1' WHERE parent_id='" . intval( $task[ 'id' ] ) . "'" );
                $ids = $query->rows;
            }

        }
        return $ids;
    }

    public function setUncompleted( $task ) {
        $ids = [];

        if ( !$task[ 'parent_id' ] ) {
            $query = $this->db->query( "SELECT id FROM tasks WHERE parent_id='" . intval( $task[ 'id' ] ) . "'" );
            if ( count( $query->rows ) ) {
                $this->db->query( "UPDATE tasks SET is_completed='0' WHERE parent_id='" . intval( $task[ 'id' ] ) . "'" );
                $ids = $query->rows;
            }
        }
        return $ids;
    }

    public function checkCompletedParent( $id ) {
        $query = $this->db->query( "SELECT * FROM tasks WHERE parent_id='" . intval( $id ) . "' AND is_completed=0" );
        if ( count( $query->rows ) == 0 ) {
            $query = $this->db->query( "SELECT * FROM tasks WHERE id='" . intval( $id ) . "' AND is_completed=0" );
            if ( $query->num_rows ) {
                $this->db->query( "UPDATE tasks SET is_completed='1' WHERE id='" . intval( $id ) . "'" );
                return $id;
            }

        }
        return [];
    }

    public function getTask( $id ) {
        $query = $this->db->query( "SELECT * FROM tasks WHERE id='" . intval( $id ) . "'" );
        return $query->row;
    }

    public function getTasksTree( $user_id = false, $only_comleated = false ) {
        $data    = [];
        $parents = $this->getParents( $user_id, $only_comleated );

        foreach ( $parents as $parent ) {
            $parent[ 'children' ] = $this->getChildren( $parent[ 'id' ] );
            $data[]               = $parent;
        }
        return $data;
    }

    /** Get children of task
     * @param $id integer
     * @return array
     */
    public function getChildren( $id ) {
        $query = $this->db->query( "SELECT * FROM tasks WHERE parent_id='" . $id . "' ORDER BY complete_till DESC" );
        return $query->rows;
    }

    /** Get parents of task
     * @param bool $user_id
     * @return mixed
     */
    public function getParents( $user_id = false ) {
        if ( $user_id ) {
            $query = $this->db->query( "SELECT *,(SELECT name FROM `users` WHERE users.user_id = tasks.user_id) AS user_name FROM tasks WHERE parent_id = 0 AND user_id='" . intval( $user_id ) . "' ORDER BY complete_till DESC" );
        } else {
            $query = $this->db->query( "SELECT *,(SELECT name FROM `users` WHERE users.user_id = tasks.user_id) AS user_name FROM tasks WHERE parent_id = 0 ORDER BY complete_till DESC" );
        }


        return $query->rows;
    }

    /** Build a tree of tasks
     * @param array $elements
     * @param int $parentId
     * @return array
     */

    function buildTree( array &$elements, $parentId = 0 ) {
        $branch = array();

        foreach ( $elements as $element ) {
            if ( $element[ 'parent_id' ] == $parentId ) {
                $children = $this->buildTree( $elements, $element[ 'id' ] );
                if ( $children ) {
                    $element[ 'children' ] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }

}
