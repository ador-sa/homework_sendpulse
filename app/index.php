<?php

error_reporting( E_ALL );
ini_set( 'display_errors', 1 );

spl_autoload_register( function ( $class_name ) {
    $file = $class_name . '.php';
    if ( file_exists( 'Controllers/' . $file ) ) {
        require_once 'Controllers/' . $file;
    } elseif ( file_exists( 'Models/' . $file ) ) {
        require_once 'Models/' . $file;
    }
} );

session_start();
require_once( 'Core/Route.php' );
require_once( 'Core/DB.php' );
require_once( 'config.php' );
require_once( 'routes.php' );
