<?php include 'Header.tpl' ?>
<h1>Описание задачи</h1>
<ul class="list-group list-group-flush">
    <li class="list-group-item">1. Возможность регистрации/логина пользователя. Для логина использовать валидный
        email.
    </li>
    <li class="list-group-item">2. Добавление/редактирование/удаление/пометка выполненных записей</li>

    <li class="list-group-item"><p>3. Запись в себя включает:</p>
        <p>&nbsp дата - не может быть меньше текущей</p>
        <p>&nbsp заголовок - не может быть пустым</p>
        <p>&nbsp тело - не может быть пустым</p>
        <p>&nbsp также запись может содержать подзадачи, которые удаляются/помечаются выполненными, если такое действие
        производят над родительской записью. И если все подзадачи выполнены, то родительская тоже помечается как
        выполненная.</p>

    </li>
    <li class="list-group-item">4. Сортировка записей происходит по времени на которое они назначаются</li>
    <li class="list-group-item">5. Использование PHP фреймворков не разрешено, однако вы можете пользоваться
        packagist
    </li>
    <li class="list-group-item">6. CSS/JavaScript библиотеки на ваш выбор</li>
    <li class="list-group-item">7. База данных – MySQL</li>
    <li class="list-group-item">8. Unit-тесты на бизнес-логику</li>
    <li class="list-group-item">9. Приложение завернуто в docker контейнер и поставляется с docker-compose.yml и
        соответствующими файлами
    </li>
    <li class="list-group-item">10. Выгружено на GitHub, GitLab, Bitbucket.org или куда вам удобнее с описанием как
        его развернуть
    </li>
</ul>


<?php include 'Footer.tpl' ?>