<?php

Route::set( '', function () {
    Controller::view( 'Home' );
} );

Route::set( 'register', function () {

    $auth = new Register();
    $auth->index();
} );

Route::set( 'login', function () {
    $auth = new Auth();
    $auth->loginForm();
} );

Route::set( 'logout', function () {
    $auth = new Auth();
    $auth->logout();
} );

/*
 * User List
 */
Route::set( 'users', function () {
    $users = new Users();
    $users->usersList();
} );
/*
 * Current user's tasks
 */
Route::set( 'user', function ( $id ) {
    $users = new Users();
    $users->user( $id );
} );

/*
 * Global task list
 */
Route::set( 'tasks', function () {
    $tasks = new Tasks();
    $tasks->getTasksList();
} );
/*
 * creating task
 */
Route::set( 'create-task', function () {
    $tasks = new Tasks();
    $tasks->create();
} );
/*
 * updatind task
 */
Route::set( 'task', function ( $id ) {
    $tasks = new Tasks();
    $tasks->update( $id );
} );
Route::set( 'task-complete', function ( $id ) {
    $tasks = new Tasks();
    $tasks->complete( $id );
} );


//Route::set('task');
