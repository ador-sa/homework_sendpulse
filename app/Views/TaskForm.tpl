<?php include 'Header.tpl' ?>

<h1><?php echo $title; ?></h1>
<form action="/<?php echo $action; ?>" method="post">
    <div class="form-group">
        <label>Название задачи*</label>
        <input type="text" name="name" class="form-control" value="<?php if(!empty($task)) echo $task['name']; ?>">
    </div>
    <div class="form-group">
        <label>Родительская задача</label>
        <select class="form-control" name="parent_id">
            <option value="">Нет</option>
            <?php foreach($parent_tasks as $parent){ ?>

            <?php if(!empty($task) && $task['id'] == $parent['id']) continue; ?>


            <?php if(!empty($task) && $task['parent_id'] == $parent['id']){ ?>
                <option value="<?php echo $parent['id']; ?>" selected="selected"><?php echo $parent['name']; ?></option>
            <?php }else{ ?>
            <option value="<?php echo $parent['id']; ?>"><?php echo $parent['name']; ?></option>
            <?php } ?>

            <?php } ?>
        </select>
    </div>
    <div class="form-group">
        <label>Текст задачи*</label>
        <textarea class="form-control" rows="5" name="body"><?php if(!empty($task)) echo $task['body']; ?></textarea>
    </div>
    <div class="form-group">
        <label>Ответственный</label>
        <select class="form-control" name="user_id">
            <option value="">Нет</option>
            <?php foreach($users as $user){ ?>
            <?php if(!empty($task) && $task['user_id'] == $user['user_id']){ ?>
            <option value="<?php echo $user['user_id']; ?>" selected="selected"><?php echo $user['name']; ?></option>
            <?php }else{ ?>
            <option value="<?php echo $user['user_id']; ?>"><?php echo $user['name']; ?></option>
            <?php } ?>
            <?php } ?>
        </select>
    </div>
    <div class="form-group">
        <label>Выполнить до*</label>
        <input type="text" name="complete_till" class="form-control date"
               value="<?php if(!empty($task)) echo $task['complete_till']; ?>">
    </div>
    <div class="form-group form-check">
        <input type="checkbox" class="form-check-input" id="exampleCheck1"
               name="is_completed" <?php if(!empty($task) && $task['is_completed']) echo 'checked'; ?>>
        <label class="form-check-label" for="exampleCheck1">Выполнена</label>
    </div>
    <button type="submit" class="btn btn-primary">Сохранить</button>
</form>

<?php include 'Footer.tpl' ?>