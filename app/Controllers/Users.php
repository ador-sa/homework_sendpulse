<?php

class Users extends Controller {

    public $userModel;
    public $taskModel;

    public function __construct() {
        parent::__construct();
        $this->userModel = new UserModel();
        $this->taskModel = new TaskModel();
    }

    /**
     * list of all users
     */
    public function usersList() {
        $data[ 'users' ] = $this->userModel->getUsers();

        self::view( 'UsersList', $data );
    }

    /**
     * tasks and subtasks of selected users
     */
    public function user( $id ) {
        $data[ 'user' ]  = $this->userModel->getUser( $id );
        $data[ 'tasks' ] = $this->taskModel->getTasksTree( $id );
        self::view( 'User', $data );
    }

}