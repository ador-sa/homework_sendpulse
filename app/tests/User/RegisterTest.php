<?php

use PHPUnit\Framework\TestCase;

class RegisterTest extends TestCase {
    /**
     * @dataProvider registerProvider
     */
    public function testValidation( $data ) {
        require_once "./config.php";

        $controller = new Register();
        $result     = $controller->validate( $data );
        $result     = $controller->validate( $data );
        $text       = !empty( $controller->errors ) ? current( $controller->errors ) : '';

        $this->assertTrue( $result, $text );
    }

    public function registerProvider() {
        $data = [
            [
                [
                    'name'     => 'Иванов',
                    'email'    => 'email@email.com',
                    'password' => '1234',
                ]
            ],
            [
                [
                    'name'     => 'Billy',
                    'email'    => 'email@email.ua',
                    'password' => 'qweqweqweqwe',
                ]
            ],
            [
                [
                    'name'     => 'Сидоров',
                    'email'    => 'some-email@gmail.com',
                    'password' => 'qweqweqwe!@#!#%%#^%',
                ]
            ]
        ];

        return $data;
    }

}
