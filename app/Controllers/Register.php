<?php

class Register extends Controller {
    public $errors = [];
    public $userModel;

    public function __construct() {
        parent::__construct();
        $this->userModel = new UserModel();
    }

    /** Show registration form and register user
     *
     */
    public function index() {

        $data = [];
        if ( $_SERVER[ 'REQUEST_METHOD' ] == "POST" ) {
            if ( $this->validate( $_POST ) ) {
                $user                    = $this->userModel->createUser( $_POST );
                $_SESSION[ 'AUTH_USER' ] = $user;
                header( "Location: http://" . $_SERVER[ 'HTTP_HOST' ] );
            }
        }
        if ( !empty( $this->errors ) ) {
            $data[ 'errors' ] = $this->errors;
        }

        self::view( 'RegisterForm', $data );
    }

    public function validate( $data ) {

        if ( empty( trim( $this->db->escape( $data[ 'name' ] ) ) ) ) {
            $this->errors[ 'name' ] = 'Имя введено не корректно';
        }

        if ( !preg_match( '/^[^\@]+@.*.[a-z]{2,15}$/i', $data[ 'email' ] ) ) {
            $this->errors[ 'email' ] = 'Email введен не корректно';
        }

        if ( $this->userModel->findUserbyEmail( $data ) ) {
            $this->errors[ 'email' ] = 'Такой емейл уже занят';
        }

        if ( strlen( trim( $this->db->escape( $data[ 'password' ] ) ) ) < 3 ) {
            $this->errors[ 'password' ] = 'Пароль должен быть не менее 3 символов';
        }

        return empty( $this->errors );
    }
}
