<?php

use PHPUnit\Framework\TestCase;

class TasksTest extends TestCase {
    /**
     * @dataProvider registerProvider
     */
    public function testValidation( $data ) {
        require_once "./config.php";

        $controller = new Tasks();
        $result     = $controller->validate( $data );
        $text       = !empty( $controller->errors ) ? current( $controller->errors ) : '';

        $this->assertTrue( $result, $text );
    }

    public function registerProvider() {
        $data = [
            [
                [
                    'name'      => 'Task # 1',
                    'body'      => 'some text of the task',
                    'parent_id' => '1',
                ]
            ],
            [
                [
                    'name'      => 'Task # 2',
                    'body'      => 'other text',
                    'parent_id' => '1234',
                ]
            ],
            [
                [
                    'name'      => 'Task # 3',
                    'body'      => 'and other text',
                    'parent_id' => '1234',
                ]
            ]
        ];

        return $data;
    }

}
