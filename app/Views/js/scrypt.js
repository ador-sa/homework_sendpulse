$('input.date').datepicker({
    format: "yyyy-mm-dd"
});


$('input.ajax-complete').on('click', function () {

    let id = $(this).data('id');
    data = {'is_completed' : $(this).prop('checked') ? 1 : 0};
    $.ajax({
        type: "POST",
        url: "/task-complete/" + id,
        data: data,
        dataType: 'json',
        success:function (data) {
            if(data.completed){
              $.each(data.completed, function(i, j){
                $($.find('input.ajax-complete[data-id='+j.id+']')).prop('checked', 'checked')
              })
            }
            if(data.uncompleted){

              $.each(data.uncompleted, function(i, j){
                $($.find('input.ajax-complete[data-id='+j.id+']')).prop('checked', false)
              })
            }

        }
    });

})
