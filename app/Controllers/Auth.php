<?php

class Auth extends Controller {

    public $userModel;

    public function __construct() {
        parent::__construct();
        $this->userModel = new UserModel();
    }

    /** Show login form and authorise
     *
     */
    public function loginForm() {
        $data = [];
        if ( $_SERVER[ 'REQUEST_METHOD' ] == "POST" ) {
            if ( $this->validate( $_POST ) ) {
                if ( $user = $this->userModel->login( $_POST ) ) {

                    $_SESSION[ 'AUTH_USER' ] = $user;
                    header( "Location: http://" . $_SERVER[ 'HTTP_HOST' ] );
                } else {
                    $this->errors[ 'email' ] = 'Пользователь с таким емейлом и паролем не найден';
                }
            }


        }

        if ( !empty( $this->errors ) ) {
            $data[ 'errors' ] = $this->errors;
        }

        self::view( 'LoginForm', $data );
    }

    public function logout() {
        $_SESSION[ 'AUTH_USER' ] = null;
        session_unset();
        header( "Location: http://" . $_SERVER[ 'HTTP_HOST' ] );

    }

    private function validate( $data ) {

        if ( !preg_match( '/^[^\@]+@.*.[a-z]{2,15}$/i', $data[ 'email' ] ) ) {
            $this->errors[ 'email' ] = 'Email введен не корректно';
        }


        if ( strlen( trim( $this->db->escape( $data[ 'password' ] ) ) ) < 3 ) {
            $this->errors[ 'password' ] = 'Пароль должен быть не менее 3 символов';
        }

        return empty( $this->errors );
    }

}
