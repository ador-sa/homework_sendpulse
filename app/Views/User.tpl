<?php include 'Header.tpl' ?>

<h1>Пользователь: <?php echo $user['name']; ?></h1>
<div class="accordion" id="accordionExample">
    <?php foreach($tasks as $task){ ?>
    <div class="card">
        <div class="card-header" id="heading<? echo $task['id']?>">
            <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapse<? echo $task['id']?>" aria-expanded="true" aria-controls="collapse<? echo $task['id']?>">

                <div class="row">
                    <div class="col-md-4">
                        <a href="/user/<? echo $task['user_id']?>"><? echo $task['user_name']?></a>
                    </div>
                    <div class="col-md-4">
                        <a href="/task/<? echo $task['id']?>"><? echo $task['name']?></a>
                    </div>
                    <div class="col-md-2">
                        <? echo $task['complete_till']?>
                    </div>
                    <div class="col-md-2">
                        <input type="checkbox" class="form-check-input ajax-complete"  data-id="<? echo $task['id']?>" <?php if($task['is_completed']) echo 'checked'; ?>>
                    </div>
                </div>

            </button>

        </div>

        <div id="collapse<? echo $task['id']?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
            <div class="card-body">
                <?php if (!empty($task['children'])){ ?>
                <?php foreach($task['children'] as $child){ ?>
                <div class="row">
                    <div class="col-md-4">

                    </div>
                    <div class="col-md-4">
                        <a href="/task/<? echo $task['id']?>"><? echo $child['name']?></a>
                    </div>
                    <div class="col-md-2">
                        <? echo $child['complete_till']?>
                    </div>
                    <div class="col-md-2">
                        <input type="checkbox" class="form-check-input ajax-complete"  data-id="<? echo $child['id']?>" <?php if(!empty($child) && $child['is_completed']) echo 'checked'; ?>>
                    </div>
                </div>
                <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php } ?>
</div>
<?php include 'Footer.tpl' ?>