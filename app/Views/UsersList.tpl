<?php include 'Header.tpl' ?>

<?php  if(!empty($users)){ ?>
    <h1>Список пользователей</h1>
    <?php foreach($users as $user){ ?>
        <ul class="list-group list-group-flush">

            <li class="list-group-item">
                <a href="/user/<?php echo $user['user_id'] ?>"><?php echo $user['name'] ?> </a>
            </li>

        </ul>
    <?php } ?>

<?php }else{ ?>
    <h1>Список пользователей пуст</h1>
<?php } ?>

<?php include 'Footer.tpl' ?>
