<?php

class UserModel extends Model {
    public function createUser( $data ) {
        $this->db->query( "INSERT INTO users SET name='" . $this->db->escape( $data[ 'name' ] ) . "', email='" . $this->db->escape( $data[ 'email' ] ) . "', password='" . $this->db->escape( sha1( $data[ 'password' ] ) ) . "'" );
        $query = $this->db->query( "SELECT * FROM users WHERE user_id='" . $this->db->getLastId() . "'" );
        return $query->row;
    }

    public function getUser( $id ) {

        $query = $this->db->query( "SELECT * FROM users WHERE user_id = '" . intval( $id ) . "'" );

        return $query->row;
    }

    public function getUsers() {
        $query = $this->db->query( 'SELECT * FROM users' );
        return $query->rows;
    }

    public function findUserbyEmail( $data ) {
        if ( !empty( $data[ 'email' ] ) ) {
            $query = $this->db->query( "SELECT * FROM users WHERE email = '" . $this->db->escape( $data[ 'email' ] ) . "'" );
            return $query->row;
        } else {
            return false;
        }

    }

    public function login( $data ) {

        $query = $this->db->query( "SELECT * FROM users WHERE email = '" . $this->db->escape( $data[ 'email' ] ) . "' AND password='" . $this->db->escape( sha1( $data[ 'password' ] ) ) . "'" );

        return $query->row;

    }
}
