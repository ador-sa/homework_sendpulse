<?php

class Tasks extends Controller {

    public $userModel;
    public $taskModel;
    public $errors = [];

    public function __construct() {
        parent::__construct();
        $this->userModel = new UserModel();
        $this->taskModel = new TaskModel();
    }


    public function getTasksList() {
        $data            = [];
        $data[ 'tasks' ] = $this->taskModel->getTasksTree();

        self::view( 'TasksList', $data );
    }

    /** Create task form and create task
     *
     */
    public function create() {

        $data                   = [];
        $data[ 'action' ]       = 'create-task';
        $data[ 'title' ]        = 'Создание задачи';
        $data[ 'users' ]        = $this->userModel->getUsers();
        $data[ 'parent_tasks' ] = $this->taskModel->getParents();

        if ( $_SERVER[ 'REQUEST_METHOD' ] == "POST" ) {
            if ( $this->validate( $_POST ) ) {
                $data                   = $_POST;
                $data[ 'is_completed' ] = isset( $_POST[ 'is_completed' ] ) ? 1 : 0;
                $this->taskModel->create( $data );
                header( "Location: http://" . $_SERVER[ 'HTTP_HOST' ] );
            }
        }

        if ( !empty( $this->errors ) ) {
            $data[ 'errors' ] = $this->errors;
        }
        self::view( 'TaskForm', $data );
    }

    public function update( $id ) {

        $data                   = [];
        $data[ 'action' ]       = 'task/' . $id;
        $data[ 'title' ]        = 'Редактирование задачи';
        $data[ 'users' ]        = $this->userModel->getUsers();
        $data[ 'parent_tasks' ] = $this->taskModel->getParents();
        $data[ 'task' ]         = $this->taskModel->getTask( $id );

        if ( $_SERVER[ 'REQUEST_METHOD' ] == "POST" ) {
            if ( $this->validate( $_POST, $id ) ) {
                $data = $_POST;
                if ( $id && !empty( $data[ 'parent_id' ] ) ) {
                    $children = $this->taskModel->getChildren( $id );
                    if ( !empty( $children ) ) {

                        $this->errors[ 'parent_id' ] = 'Прежде нужно убрать подзадачи этой задачи';
                    }
                    $data[ 'user_id' ] = 0;
                }
                $data[ 'is_completed' ] = isset( $_POST[ 'is_completed' ] ) ? 1 : 0;
                $this->taskModel->update( $id, $data );
                header( "Location: http://" . $_SERVER[ 'HTTP_HOST' ] );


            }
        }

        if ( !empty( $this->errors ) ) {
            $data[ 'errors' ] = $this->errors;
        }
        self::view( 'TaskForm', $data );
    }

    /** validate task form
     * @param $data array
     * @return bool
     */
    public function validate( $data ) {
        if ( empty( trim( $this->db->escape( $data[ 'name' ] ) ) ) ) {
            $this->errors[ 'name' ] = 'Название введено не корректно';
        }
        if ( empty( trim( $this->db->escape( $data[ 'body' ] ) ) ) ) {
            $this->errors[ 'body' ] = 'Текст введен не корректно';
        }
        if ( empty( trim( $this->db->escape( $data[ 'body' ] ) ) ) ) {
            $this->errors[ 'body' ] = 'Текст введен не корректно';
        }


        return empty( $this->errors );
    }

    /** ajax function to set complete status
     * @param $id integer
     */

    public function complete( $id ) {

        if ( $_SERVER[ 'REQUEST_METHOD' ] == "POST" && isset( $_POST[ 'is_completed' ] ) ) {

            $toched_tasks = $this->taskModel->complete( $id, $_POST[ 'is_completed' ] );
            if ( $toched_tasks ) {
                $this->return_json( $toched_tasks );
            }
        }

    }

}
