<?php

class Controller {
    public $db;

    public function __construct() {
        $this->db = new DB();
    }

    public static function view( $view, $data = [] ) {
        if ( !empty( $data ) ) {
            extract( $data );
        }
        require_once( 'Views/' . $view . '.tpl' );
    }

    public function return_json( $data ) {
        header( 'Content-type: application/json' );
        echo json_encode( $data );
    }
}
